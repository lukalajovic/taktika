#sem dam vse funkcije ki ne spadajo direktno v program in se jih da tudi v drugih programih uporabljati
def jevkvadratu(x,y,mnozica):
    q=False
    for i in mnozica:
        if x>i[0] and x<i[2] and y>i[1] and y<i[3]:
            q=True
            break
    return q
def jk(x,y,mnozica):
    q=False
    for i in mnozica:
        if x>i[0] and x<i[2] and y>i[1] and y<i[3]:
            q=i
            break
    return q
def dijkstra(g, zac, kon):
    prev = {v: None for v in g}
    dist = {v: float('inf') for v in g}
    dist[zac] = 0

    vrsta = list(g.keys())
    while vrsta:
        _, najblizji = min((dist[v], v) for v in vrsta)
        vrsta.remove(najblizji)
        for u, d in g[najblizji].items():
            if dist[najblizji] + d < dist[u]:
                dist[u] = dist[najblizji] + d
                prev[u] = najblizji

    pot = [kon]
    while kon != zac:
        pot.append(prev[kon])
        kon = prev[kon]

    return list(reversed(pot))

def dolgapot(graf):
    a={}
    b={}
    c={}
    c=0
    for i in graf:
        for j in graf[i]:
            b[j]=0
        a[i]=b
        b={}
    return a
def pot(g,zac,kon):
    return dijkstra(dolgapot(g),zac,kon)
    
def minlen(seznami):
    if len (seznami)==[0]:
        return []
    kratek=seznami[0]
    
    if len(seznami)==1:
        return seznami[0]
    a=1
    while a<len(seznami)-1:
        if len (seznami)[a]<kratek:
            kratek=seznami[a]
        a+=1



