#tole je igra za 2 igralca
import slika2
#slika 2 je mapa od evrope
import svet
#svet je mapa od riska je slabse narejena a bo pomojem hitreje delala
import zfunkcije
# zfunkcije so zunanje funkcije, funkcije ki bi se jih dalo uporabiti za druge namene nekatere se tudi je
from tkinter import *
import random
    
root=Tk()
#tukaj sta razreda z zemljevidi sepravi slikami, koordinatami in grafi ki kazejo povezave med drzavami
class Mappicaa:
    kordinate={'Svedska':slika2.Svedska,'Avstrija':slika2.Avstrija,'Spanija':slika2.Spanija,'Italija':slika2.Italija,'Belgija':slika2.Belgija,'Portugalska':slika2.Portugalska,'Nizozemska':slika2.Nizozemska,'Ciper':slika2.Ciper+[[750,818,796,839]],'Luksmburg':slika2.Luksmburg+[[289,386,341, 450]],'Finska':slika2.Finska,'Hrvaska':slika2.Hrvaska,'Litva':slika2.Litva,'Nemcija':slika2.Nemcija,'Malta':slika2.Malta+[[371, 818,429, 845]],'Madarska':slika2.Madarska,'Irska':slika2.Irska,'Poljska':slika2.Poljska,'Danska':slika2.Danska,'Romunija':slika2.Romunija,'Francija':slika2.Francija,'Slovaska':slika2.Slovaska,'Grcija':slika2.Grcija,'VB':slika2.Britanija,'Latvija':slika2.Latvia,'Slovenija':slika2.Slovenija,'Estonija':slika2.Estonia,'Bolgarija':slika2.Bolgarija,'Ceska':slika2.Ceska}
    Graf=slika2.Graf
    foto=PhotoImage(file = 'map.gif')
class Riskmapa:
    kordinate=svet.kordinate
    Graf=svet.Graf

    foto=PhotoImage(file = 'riskss.gif')

prestol=["Ciper","Portugalska","Finska","Irska"]
prestol2=['indonezija','svteritorij','oral','madagaskar']
random.shuffle(prestol)
random.shuffle(prestol2)

class Risk():


    #to sta razreda igralcev modr je clovek rdec je racunalnik
    class Modra():
        barva="blue"
        prestolnica=prestol[0]
        polja={prestolnica:(5,0)}
        vojaki=5
        utrujeni=0    
    class Rdeca():
        prestolnica=prestol[1]
        barva="red"
        polja={prestolnica:(15,0)}
        vojaki=5
        utrujeni=0

    class Vijolcna():
        prestolnica=prestol[2]
        barva="purple"
        polja={prestolnica:(15,0)}
        vojaki=5
        utrujeni=0
        
    class Zlata():
        prestolnica=prestol[3]
        barva="gold"
        polja={prestolnica:(15,0)}
        vojaki=5
        utrujeni=0
        
    strund=1
    #ce je clovek true lahko napadamo drugace je navrsti racunalnik
    clovek=False
    
    #ko bo zemljevid izbran bo postal true in funkcije ki dolocajo zemljevid ne bodo uporabne
    
    def __init__(self,master):
        self.navodila = ["""cilj igre je da osvojiš sver oziroma osvojiš čim več ozemelj v 30ih potezah,""",
                        """vsako igro dobiš nekaj vojakov (države/2+1), rundo začneš tako da z levo tipko miške pritisneš na tisto polje kamor boš dal okrepitve ta se ti obarva z srebrno.""",
                        """z desno tipko označiš državo iz katere se boš premikal, ali pa preveriš koliko vojakov je na njej""",
                        """Iz vsakega ozemlja lahko osvajaš le sosednja ozemlja lahko tudi premikaš vojake po svojih ozemljih.""",
                        """V vsakem ozemlju mora biti vsaj 1 vojak zato lahko napadas z vsemi razen enim.""",
                        """ """,
                        """če napadaš nasprotnika mečeš kocke če imaš enega napadalca imaš eno kocko 2==2 če imaš več kot 2 dobiš 3 kocke""",
                        """branilec ima eno ali dve kocki če vrže več ali enako kot nasprotnikove najbolše kocke ga premaga""",
                        """za vsako državo piše koliko ima vojakov gor in koliko je utrujenih, utrujeni ne morejo napadati""",
                        """igra se konča ko nekdo zavzame veliko večino sveta ali pa ko mine 30' rund""",
                         """ pazi računalnik lahko gulfa """,
                         """rundo končaš tako da pritisneš končaj """,
                         """ številka zgoraj  zraven znaka + in izberi vse ti pove koliko vojakov je pripravljenih za napad."""
                         
                  ]

        self.mappa=Mappicaa()

        #rabimo vedeti koloko vojakov je nepremaknjenih ce je to st enako 0 potem je naslednji na vrsti naslednji igralec
        self.platno=Canvas(master,width=1000,height=1000,bg="lightgreen")
        
        self.platno.grid(row=4,column=4)

        
        
        self.NAVRSTI=self.Modra
        
        self.NENAVRSTI=self.Rdeca

        self.NENAVRSTI2=self.Vijolcna

        self.NENAVRSTI3=self.Zlata
        
        self.vojaki_za_napad=0
        #tale ti pove koliko vojakov lahki napade iz tega polja
        self.izbrana_drzava=self.NAVRSTI.prestolnica
        
        self.napis_drzava=StringVar(value=str(self.izbrana_drzava))
        
        labeldrzava=Label(master,textvariable=self.napis_drzava)
        
        labeldrzava.grid(row=1,column=1)
        #to je drzava ki jo izberemo z levim gumbom
        self.stevec=0
        
        gumb_povecaj = Button(master, text=" +1 ", command=self.povecaj)
        
        gumb_povecaj.grid(row=0, column=2)

        gumb_vse=Button(master,text="izberi vse",fg="orange",command=self.izberi_vse)

        gumb_vse.grid(row=0,column=3)
        
        self.napis_stevec = StringVar(value=str(self.stevec))
        
        label_stevec = Label(master, textvariable=self.napis_stevec)
        
        label_stevec.grid(row=2, column=2)

        self.srund=StringVar(value="runda"+str(self.strund))

        runde=Label(master,textvariable=self.srund)

        runde.grid(row=1,column=4)

        self.vojaki_napolju=(0,0)
        
        self.vojaki_naizbiro=0
        
        self.napis_stevec2 = StringVar(value=str(self.vojaki_napolju))
        
        label_stevec2 = Label(master, textvariable=self.napis_stevec2)
        
        label_stevec2.grid(row=3, column=3)
        
        
        self.platno.bind("<Button-3>", self.izbranopolje)
        
        
        self.platno.bind("<Button-1>",self.cnapad)
        
        gumb_konc = Button(master, text="KONCAJ!", command=self.konecc)
        
        gumb_konc.grid(row=1, column=5)
        

        
        menu=Menu(master)
        master.config(menu=menu)
        meni1=Menu(menu)
        meni2=Menu(menu)
        meni5=Menu(menu)

        menu.add_cascade(label="nova igra",menu=meni1)
        meni1.add_command(label="zemljevid1",command=lambda:self.zemljevid1())
        meni1.add_command(label="zemljevid2",command=lambda:self.zemljevid2())

        menu.add_cascade(label="izhod",menu=meni2)
        meni2.add_command(label="izhod",command=master.destroy)

        menu.add_cascade(label="Navodila", menu=meni5)
        meni5.add_command(label="Pokaži navodila", command=lambda: self.narisi_navodila())
        meni5.add_command(label="zbrisi navodila",command=lambda: self.zbrisi_navodila())





        self.napis_barva = StringVar(value=str(self.NAVRSTI.barva))
        
        label_napis_barva = Label(master, textvariable=self.napis_barva)
        
        label_napis_barva.grid(row=4, column=1)

        self.stpolj=StringVar(value=str(len(self.NAVRSTI.polja))+str(self.NAVRSTI.barva)+str(len(self.NENAVRSTI.polja))+str(self.NENAVRSTI.barva)+str(len(self.NENAVRSTI2.polja))+str(self.NENAVRSTI2.barva)+str(len(self.NENAVRSTI3.polja))+str(self.NENAVRSTI3.barva))
        
        labelpolja=Label(master,textvariable=self.stpolj)

        labelpolja.grid(row=2,column=4)

        self.bitka=StringVar(value=str((0,0))+" rezultatnapada")

        labelbitka=Label(master,textvariable=self.bitka,fg="darkgreen")

        labelbitka.grid(row=3,column=4)
        
        self.narisi_navodila()

    def narisi_navodila(self):
        """izpiše navodila na canvas"""
        #self.napis.set(" ")
        self.platno.delete(ALL)
        for i in range(len(self.navodila)):
            self.platno.create_text(500, i*20 + 40, text=self.navodila[i],fill="black")
    def zbrisi_navodila(self):
        self.platno.delete(ALL)
        self.platno.create_image(500,500,image=self.mappa.foto)
        self.podatki()
        self.barve_drzav()
        

        

    def prestavi(self, event):
        '''Prestavi krogec tja, kjer je miška.'''
        (x,y) = (event.x, event.y)
        self.platno.coords(self.krogec, x-5, y-5, x+5, y+5)

    
    def sks(self,u,barva):
        for j in u:
            self.platno.create_rectangle(j[0],j[1],j[2],j[3],fill=barva,outline=barva)

    def barve_drzav(self):
        self.platno.delete("all")
        self.platno.create_image(500,500,image=self.mappa.foto)
        self.podatki()
        for i in self.mappa.kordinate:
            if i in self.NAVRSTI.polja:
                self.sks(self.mappa.kordinate[i],self.NAVRSTI.barva)
            elif i in self.NENAVRSTI.polja:
                self.sks(self.mappa.kordinate[i],self.NENAVRSTI.barva)
            elif i in self.NENAVRSTI2.polja:
                self.sks(self.mappa.kordinate[i],self.NENAVRSTI2.barva)
            elif i in self.NENAVRSTI3.polja:
                self.sks(self.mappa.kordinate[i],self.NENAVRSTI3.barva)
            else:
                self.sks(self.mappa.kordinate[i],"lightgreen")
                
    def dvedrzavi(self,dr1):
        i=dr1
        if i in self.NAVRSTI.polja:
            self.sks(self.mappa.kordinate[i],self.NAVRSTI.barva)
        elif i in self.NENAVRSTI.polja:
            self.sks(self.mappa.kordinate[i],self.NENAVRSTI.barva)
        elif i in self.NENAVRSTI2.polja:
            self.sks(self.mappa.kordinate[i],self.NENAVRSTI2.barva)
        elif i in self.NENAVRSTI3.polja:
            self.sks(self.mappa.kordinate[i],self.NENAVRSTI3.barva)
        else:
            self.sks(self.mappa.kordinate[i],"lightgreen")
   

            
                
    def podatki(self):
        a=10
        b=40
        self.platno.create_rectangle(35,10,(len(self.mappa.kordinate)-23)*100,100,fill="white")
        for i in self.mappa.kordinate:
            if i in self.NAVRSTI.polja:
                self.platno.create_text(b,a,text=i+" | "+str(self.NAVRSTI.polja[i]),anchor=NW,fill=self.NAVRSTI.barva, font=('Arial', 8, 'bold italic'))
            elif i in self.NENAVRSTI.polja:
                self.platno.create_text(b,a,text=i+" | "+str(self.NENAVRSTI.polja[i]),anchor=NW,fill=self.NENAVRSTI.barva, font=('Arial', 8, 'bold italic'))
            elif i in self.NENAVRSTI2.polja:
                self.platno.create_text(b,a,text=i+" | "+str(self.NENAVRSTI2.polja[i]),anchor=NW,fill=self.NENAVRSTI2.barva, font=('Arial', 8, 'bold italic'))
            elif i in self.NENAVRSTI3.polja:
                self.platno.create_text(b,a,text=i+" | "+str(self.NENAVRSTI3.polja[i]),anchor=NW,fill=self.NENAVRSTI3.barva, font=('Arial', 8, 'bold italic'))
            
            else:
                self.platno.create_text(b,a,text=i+" | "+str(0),anchor=NW,fill='black', font=('Arial', 8, 'bold italic'))
            a+=10
            
            if a==90:
                a=10
                b+=120
                
    def izberi_vse(self):
        if self.clovek==True:
            self.stevec=self.vojaki_za_napad
            self.napis_stevec.set(str(self.stevec)) 
                
    def povecaj(self):
        if self.clovek==True:
            if self.vojaki_za_napad<=self.stevec:
                self.stevec=0
            else:
                self.stevec = self.stevec+ 1
            self.napis_stevec.set(str(self.stevec))        
     #dodaj se da so vsi utrujeni enaki 0


    # ta funkcija bo zacela igro saj bo sodelovala pri dveh gumbih
    def ljudje(self):
        if self.clovek==False:
            self.clovek=True
        else:
            self.clovek=False


        
        
    def konecpoteze(self):
        self.krogec = self.platno.create_oval(145, 145, 155, 155, fill="silver")
        if self.strund==30:
            self.konecigre()
        if (len(self.NAVRSTI.polja) or len(self.NENAVRSTI2.polja) or len(self.NENAVRSTI.polja) or len(self.NENAVRSTI3.polja))==len(self.mappa.Graf)-3:
            self.konecigre()
        else:
            for j in self.NAVRSTI.polja:
                self.NAVRSTI.polja[j]=(self.NAVRSTI.polja[j][0],0)
            for x in self.NENAVRSTI.polja:
                self.NENAVRSTI.polja[x]=(self.NENAVRSTI.polja[x][0],0)
            for x in self.NENAVRSTI2.polja:
                self.NENAVRSTI2.polja[x]=(self.NENAVRSTI2.polja[x][0],0)
            for x in self.NENAVRSTI3.polja:
                self.NENAVRSTI3.polja[x]=(self.NENAVRSTI3.polja[x][0],0)
            (self.NENAVRSTI3,self.NAVRSTI,self.NENAVRSTI,self.NENAVRSTI2)=(self.NAVRSTI,self.NENAVRSTI,self.NENAVRSTI2,self.NENAVRSTI3)
            if self.Modra==self.NAVRSTI:
                self.strund+=1
                self.srund.set("runda:"+str(self.strund))
            self.vojaki_naizbiro=len(self.NAVRSTI.polja)//2
            self.barve_drzav()
            self.napis_barva.set(self.NAVRSTI.barva)
            self.stpolj.set("število polj||"+str(len(self.NAVRSTI.polja))+str(self.NAVRSTI.barva)+str(len(self.NENAVRSTI.polja))+str(self.NENAVRSTI.barva)+str(len(self.NENAVRSTI2.polja))+str(self.NENAVRSTI2.barva)+str(len(self.NENAVRSTI3.polja))+str(self.NENAVRSTI3.barva))


            

    
    def metkocke(self):
        return [[random.randint(1,6),random.randint(1,6),random.randint(1,6)],[random.randint(1,6),random.randint(1,6)]]





    def konecigree(self):
        bar=self.NENAVRSTI.barva

        
        self.platno.create_text(0,400,text="zmaga",anchor=NW,fill="blue", font=('Arial', 70, 'bold italic'))
        self.platno.create_rectangle(100,100,900,900,fill="red")
        
    theend=False
    def konecigre(self):
        if self.theend==False:
            self.konecigree()
            self.theend=True
            self.platno.after(2000,self.konecigre)
        else:
            self.platno.delete("all")
            ars=q=[self.NAVRSTI.polja,self.NENAVRSTI.polja,self.NENAVRSTI2.polja,self.NENAVRSTI3.polja]
            w=q[0]
            for i in q:
                if len(i)>len(w):
                    w=i
            e=""
            if w==self.NAVRSTI.polja:
                e=self.NAVRSTI.polja
            if w==self.NENAVRSTI.polja:
                e=self.NENAVRSTI.polja            
            if w==self.NENAVRSTI2.polja:
                e=self.NENAVRSTI2.polja                    
            if w==self.NENAVRSTI3.polja:
                e=self.NENAVRSTI3.polja            
            self.platno.create_text(0,400,text="zmaga",anchor=NW,fill=e, font=('Arial', 70, 'bold italic'))
            
    

      #spopad bo povedal koliko vojakov bo umrlo
    #trenutno se imenuje spopad2 ker sem nekaj narobe naredil in moram se popraviti ko bom to opravil bom zbrisal prejsno
    def spopad(self,n,b,q):
        rezultat=(1,1)
        kn1=q[0][0]
        kn2=q[0][1]
        kn3=q[0][2]
        kb1=q[1][0]
        kb2=q[1][1]
        if n==1 and b==1:
            if kn1>kb1:
                rezultat=(0,1)
            else:
                rezultat=(1,0)
        else:
            if n==1 and b>1:
                if n>max(kb1,kb2):
                    self.platno.create_oval(750,0,800,50,fill="lightblue")
                    self.platno.create_text(800,50,text=str(kb1)+"|"+str(kb1),fill="black",font=('Arial',15, 'bold italic'))
                    rezultat=(0,1)
                else:
                    rezultat=(1,0)
            else:
                if n==2 and b==1:
                    if max(kn1,kn2)>kb1:
                        rezultat=(0,1)
                    else:
                        rezultat=(1,0)
                else:
                    if n==2 and b>1:
                        if max(kn1,kn2)>max(kb1,kb2) and min(kn1,kn2)>min(kb1,kb2):
                            rezultat=(0,2)
                        else:
                            if max(kn1,kn2)<=max(kb1,kb2) and min(kn1,kn2)<=min(kb1,kb2):
                                rezultat=(2,0)
                            else:
                                rezultat=(1,1)
                    else:
                        if n>2 and b==1:
                            if max(kn1,kn2,kn3)>kb1:
                                rezultat=(0,1)
                            else:
                                rezultat=(1,0)
                        else:
                            if n>2 and b>1:
                                a=[kn1,kn2,kn3]
                                b=[kb1,kb2]
                                a.sort()
                                b.sort()
                                
                                
                                if a[2]>b[1] and a[1]>b[0]:
                                    rezultat=(0,2)
                                else:
                                    if a[2]<=b[1] and a[1]<=b[0]:
                                        rezultat=(2,0)
                                    else:
                                        rezultat=(1,1)
        return rezultat
    

    def zemljevid1(self):
        
        self.clovek=True
        self.strund=1    
        self.mappa=Mappicaa()
        self.Modra.prestolnica=prestol[0]
        self.Modra.polja={}
        self.Modra.polja[prestol[0]]=(5,0)
        self.Rdeca.prestolnica=prestol[1]
        self.Rdeca.polja={}
        self.Rdeca.polja[prestol[1]]=(15,0)

        self.NAVRSTI=self.Modra
        self.izbrana_drzava=self.NAVRSTI.prestolnica
        self.NENAVRSTI=self.Rdeca
            
        self.Vijolcna.prestolnica=prestol[2]
        self.Vijolcna.polja={}
        self.Vijolcna.polja[prestol[2]]=(15,0)
        self.NENAVRSTI2=self.Vijolcna

        self.Zlata.prestolnica=prestol[3]
        self.Zlata.polja={}
        self.Zlata.polja[prestol[3]]=(15,0)
        self.NENAVRSTI3=self.Zlata
        self.platno.delete("all")
        self.platno.create_image(500,500,image=self.mappa.foto)            
        self.barve_drzav()
        self.podatki()
        self.srund.set("runda:"+str(self.strund))
        self.stpolj.set(str(len(self.NAVRSTI.polja))+str(self.NAVRSTI.barva)+str(len(self.NENAVRSTI.polja))+str(self.NENAVRSTI.barva)+str(len(self.NENAVRSTI2.polja))+str(self.NENAVRSTI2.barva)+str(len(self.NENAVRSTI3.polja))+str(self.NENAVRSTI3.barva))
        random.shuffle(prestol)    
    def zemljevid2(self):

        self.clovek=True
        self.strund=1    
        self.mappa=Riskmapa()
        self.Modra.prestolnica=prestol2[0]
        self.Modra.polja={}
        self.Modra.polja[prestol2[0]]=(5,0)
        self.Rdeca.prestolnica=prestol2[1]
        self.Rdeca.polja={}
        self.Rdeca.polja[prestol2[1]]=(15,0)

        self.NAVRSTI=self.Modra
        self.izbrana_drzava=self.NAVRSTI.prestolnica
        self.NENAVRSTI=self.Rdeca
            
        self.Vijolcna.prestolnica=prestol2[2]
        self.Vijolcna.polja={}
        self.Vijolcna.polja[prestol2[2]]=(15,0)
        self.NENAVRSTI2=self.Vijolcna

        self.Zlata.prestolnica=prestol2[3]
        self.Zlata.polja={}
        self.Zlata.polja[prestol2[3]]=(15,0)
        self.NENAVRSTI3=self.Zlata
        self.platno.delete("all")
        self.platno.create_image(500,500,image=self.mappa.foto)            
        self.barve_drzav()
        self.podatki()
        self.srund.set("runda:"+str(self.strund))
        self.stpolj.set("število držav pod nadzorom"+str(len(self.NAVRSTI.polja))+str(self.NAVRSTI.barva)+str(len(self.NENAVRSTI.polja))+str(self.NENAVRSTI.barva)+str(len(self.NENAVRSTI2.polja))+str(self.NENAVRSTI2.barva)+str(len(self.NENAVRSTI3.polja))+str(self.NENAVRSTI3.barva))
        random.shuffle(prestol2)
            
            
            
        
    #izbere drzavo za cloveka
    def izbranopolje(self,event):
        for i in self.mappa.kordinate:
            if zfunkcije.jevkvadratu(event.x,event.y,self.mappa.kordinate[i])==True:
                self.izbrana_drzava=i
                self.napis_drzava.set(self.izbrana_drzava)
                if i in self.NAVRSTI.polja:
                    if (self.NAVRSTI.polja[i][0]-self.NAVRSTI.polja[i][1])>=1:
                        self.vojaki_za_napad=self.NAVRSTI.polja[i][0]-self.NAVRSTI.polja[i][1]-1
                        self.stevec=0
                        self.napis_stevec.set(str(self.stevec))                 
                    else:
                        self.vojaki_za_napad=0
                    self.napis_stevec2.set("vojaki||"+str(self.NAVRSTI.polja[i])+"utrujeni vojaki")
                    break
                elif i in self.NENAVRSTI.polja:
                    self.napis_stevec2.set("vojaki||"+str(self.NENAVRSTI.polja[i])+"utrujeni vojaki")
                    self.vojaki_za_napad=0
                    self.stevec=0
                    self.napis_stevec.set(str(self.stevec)) 
                    
                elif i in self.NENAVRSTI2.polja:
                    self.napis_stevec2.set("vojaki||"+str(self.NENAVRSTI2.polja[i])+"utrujeni vojaki")
                    self.vojaki_za_napad=0
                    self.stevec=0
                    self.napis_stevec.set(str(self.stevec)) 
                elif i in self.NENAVRSTI3.polja:
                    self.napis_stevec2.set("vojaki||"+str(self.NENAVRSTI3.polja[i])+"utrujeni vojaki")
                    self.vojaki_za_napad=0
                    self.stevec=0
                    self.napis_stevec.set(str(self.stevec)) 
                else:
                    self.napis_stevec2.set(str(0))
                    self.vojaki_za_napad=0
                    self.stevec=0
                    self.napis_stevec.set(str(self.stevec)) 



    def dodajvojskoo(self,drzava):
        self.NAVRSTI.polja[drzava]=(self.NAVRSTI.polja[drzava][0]+len(self.NAVRSTI.polja)//2+1,0)
        self.podatki()
    koko=True
    def dodajvojsko(self,drzava):
        if self.koko==True:
            self.dodajvojskoo(drzava)
            self.sks(self.mappa.kordinate[drzava],"silver")
            
        

            

    def napadi(self,drzava):
        #print("alea acta est")
        t=self.izbrana_drzava
        j=self.stevec
        q=self.metkocke()
        #print("007")
        if drzava in self.NAVRSTI.polja:
            if self.stevec>0:
                self.NAVRSTI.polja[drzava]=(self.NAVRSTI.polja[drzava][0]+self.stevec,self.NAVRSTI.polja[drzava][1]+self.stevec)
                o=self.NAVRSTI.polja[self.izbrana_drzava]
                self.NAVRSTI.polja[self.izbrana_drzava]=(o[0]-self.stevec,o[1])        
        else:
            if drzava not in self.NENAVRSTI.polja and drzava not in self.NENAVRSTI2.polja and drzava not in self.NENAVRSTI3.polja:
                if self.stevec>0:
                    self.NAVRSTI.polja[drzava]=(self.stevec,self.stevec)
                    self.NAVRSTI.polja[self.izbrana_drzava]=(self.NAVRSTI.polja[self.izbrana_drzava][0]-self.stevec,self.NAVRSTI.polja[self.izbrana_drzava][1])
            else:
                if self.stevec>0:
                    if drzava in self.NENAVRSTI.polja:
                        d=self.spopad(self.stevec,self.NENAVRSTI.polja[drzava][0],q)
                        self.bitka.set(str(d)+" žrtve"+"|"+t+"||"+drzava)
                        self.NAVRSTI.polja[self.izbrana_drzava]=(self.NAVRSTI.polja[self.izbrana_drzava][0]-d[0],self.NAVRSTI.polja[self.izbrana_drzava][1])
                        self.NENAVRSTI.polja[drzava]=(self.NENAVRSTI.polja[drzava][0]-d[1],0)
                        self.stevec=0
                        if self.NENAVRSTI.polja[drzava][0]<=0:
                            del self.NENAVRSTI.polja[drzava]
                    elif drzava in self.NENAVRSTI2.polja:
                        d=self.spopad(self.stevec,self.NENAVRSTI2.polja[drzava][0],q)
                        self.bitka.set(str(d)+" žrtve"+"|"+t+"||"+drzava)
                        self.NAVRSTI.polja[self.izbrana_drzava]=(self.NAVRSTI.polja[self.izbrana_drzava][0]-d[0],self.NAVRSTI.polja[self.izbrana_drzava][1])
                        self.NENAVRSTI2.polja[drzava]=(self.NENAVRSTI2.polja[drzava][0]-d[1],0)
                        self.stevec=0
                        if self.NENAVRSTI2.polja[drzava][0]<=0:
                            del self.NENAVRSTI2.polja[drzava]
                    else:
                        d=self.spopad(self.stevec,self.NENAVRSTI3.polja[drzava][0],q)
                        self.bitka.set(str(d)+" žrtve"+"|"+t+"||"+drzava)
                        self.NAVRSTI.polja[self.izbrana_drzava]=(self.NAVRSTI.polja[self.izbrana_drzava][0]-d[0],self.NAVRSTI.polja[self.izbrana_drzava][1])
                        self.NENAVRSTI3.polja[drzava]=(self.NENAVRSTI3.polja[drzava][0]-d[1],0)
                        self.stevec=0
                        if self.NENAVRSTI3.polja[drzava][0]<=0:
                            del self.NENAVRSTI3.polja[drzava]
                    #print("tja"+str(q))
        self.stevec=0
        self.napis_stevec.set(str(self.stevec))
        self.vojaki_za_napad-=j
        self.izbrana_drzava=t
        self.napis_drzava.set(self.izbrana_drzava)
        self.napis_stevec2.set("vojaki||"+str(self.NAVRSTI.polja[self.izbrana_drzava])+"||utrujeni vojaki")
        self.dvedrzavi(drzava)
        self.podatki()
                
        
    banditi=False
    def napad(self,drzava):
        if self.banditi==False:
            self.banditi=True
            self.napadi(drzava)
            
            self.platno.after(10,self.napad(drzava))
        else:
            self.banditi=False
            
            
                                
    vojskaalinapad=True
    
    def cnapad(self,event):
        on=self.mappa.Graf[self.izbrana_drzava]
        if self.clovek==True:
            if self.izbrana_drzava==[]:
                on=self.NAVRSTI.prestolnica
            else:
                on=self.mappa.Graf[self.izbrana_drzava]
            if self.vojskaalinapad==True:
                for i in self.mappa.kordinate:
                    if zfunkcije.jevkvadratu(event.x,event.y,self.mappa.kordinate[i])==True:
                        if i in self.NAVRSTI.polja:
                                self.izbrana_drzava=i
                                self.napis_drzava.set(self.izbrana_drzava)
                                self.dodajvojsko(i)
                                self.vojaki_za_napad=self.NAVRSTI.polja[i][0]-self.NAVRSTI.polja[i][1]-1
                                self.stevec=0
                                self.napis_stevec.set(str(self.stevec))
                                self.napis_stevec2.set("vojska ||"+str(self.NAVRSTI.polja[i])+"||urtujenavojska")
                                self.vojskaalinapad=False
            else:
                
                for i in on:
                    if zfunkcije.jevkvadratu(event.x,event.y,self.mappa.kordinate[i])==True:
                        if i in self.mappa.Graf[self.izbrana_drzava]:
                            self.napad(i)
                            self.izbrana_drzava=self.izbrana_drzava
                            self.napis_drzava.set(self.izbrana_drzava)
                            self.vojaki_za_napad=self.NAVRSTI.polja[self.izbrana_drzava][0]-self.NAVRSTI.polja[self.izbrana_drzava][1]-1
                            self.stevec=0
                            self.napis_stevec.set(str(self.stevec))
                            self.napis_stevec2.set("vojska ||"+str(self.NAVRSTI.polja[self.izbrana_drzava])+"||urtujenavojska")
                            break
    
#od tuki naprej bodo več al mn metode ki upravljajo racunalnik    
    

    def stvojakov(self):
        a=0
        b=0
        for i in self.NAVRSTI.polja:
            if self.NAVRSTI.polja[i][0]>0:
                a+=self.NAVRSTI.polja[i][0]-1
            b+=self.NAVRSTI.polja[i][1]
        return (a,b)
    
    
    def seznampolji(self):
        a=[]
        for i in self.NAVRSTI.polja:
            a+=[i]
        #print(a)
        return a


    rrdeca=False
    mmodra=False
    zzlata=False
    vvijolcna=False
    #tale konca clovekovo potezo    
    def konecc(self):
        if self.clovek==True:
            if self.strund<=30:
                self.clovek=False
                self.konecpoteze()
                self.rrdeca=True
                self.rdecarunda()
                self.vojskaalinapad=True
                
    
            
    def rdecarunda(self):
        if self.rrdeca==True:
            if len(self.NAVRSTI.polja)>0:
                self.moznastrategija1()
            self.rrdeca=False
            self.konecpoteze()
            self.vvijolcna=True
            self.vijolcnarunda()
        

    
                        
    def vijolcnarunda(self):
        if self.vvijolcna==True:
            if len(self.NAVRSTI.polja)>0:
                self.moznastrategija1()

            self.vvijolcna=False
            self.konecpoteze()
            self.zzlata=True
            self.zlatarunda()
                    
                    
    def zlatarunda(self):
        if self.zzlata==True:
            if len(self.NAVRSTI.polja)>0:
                self.moznastrategija1()
            self.zzlata=False
            self.konecpoteze()
            self.clovek=True        

    def moznastrategija1(self):
        if len(self.NAVRSTI.polja)>0:
            a=self.mejnapolja(self.NAVRSTI.polja)
            if len(a)==1:
                self.dodajvojsko(a[0])
                print(self.makspoteza(self.NAVRSTI.polja,a[0]))
                self.izvedipotezo(self.makspoteza(self.NAVRSTI.polja,a[0]))
            else:
                #print(str(a)+"to so moji problemi")
                b=a[0]
                #print(b)
                c=self.NAVRSTI.polja
                for i in a:
                    if self.tocke(self.izvedenapoteza(c,self.makspoteza(c,i)))>self.tocke(self.izvedenapoteza(c,self.makspoteza(c,b))):
                        b=i
                    c=self.NAVRSTI.polja
                self.dodajvojsko(b)
                #print(self.NAVRSTI.polja)
                for i in a:
                    if c[i][0]>1:
                        print(self.makspoteza(c,i))
                        self.izvedipotezo(self.makspoteza(c,i))
        else:
            pass
                        

    
        
    
    def tocke(self,graf1):
        return len(graf1)


    def mejnapolja(self,graff):
        a=[]
        for i in graff:
            o=i
            for j in self.mappa.Graf[o]:
                
                if j not in graff:
                    a+=[o]
                    break

        
                
        
        return a

    def rezultatnapada(self,napadalci,branilci):
        return napadalci-branilci

    
    def moznepoteze(self,drzava,graf1):
        if graf1[drzava][0]>0:
            a=graf1[drzava][0]-1
            sosede=[]
            rezultat=[]
            if a==0:
                return rezultat
            else:
                for i in self.mappa.Graf[drzava]:
                    if i not in graf1:
                        sosede+=[i]

                if len(sosede)==1:
                    return [[(drzava,sosede[0],a)]]
                else:

                    if a<=len(sosede):
                        if a==len(sosede):
                            for x in sosede:
                                rezultat+=[(drzava,x,1)]
                            return [rezultat]
                        else:
                            e=0
                            f=0
                            moznapoteza=[]
                            while e<len(sosede):
                                rezultat+=[[(drzava,sosede[e],a)]]
                                e+=1
                            return rezultat

                    else:
                        b=a-len(sosede)+1
                        rezultati=[]
                        c=0
                        d=0
                        while c<len(sosede)-1:
                            while d<len(sosede)-1:
                                if c==d:
                                    rezultati+=[(drzava,sosede[d],b)]
                                else:
                                    rezultati+=[(drzava,sosede[d],1)]
                                d+=1
                            rezultat+=[rezultati]
                            rezultati=[]
                            d=0
                            c+=1

                    return rezultat
        else:
            return []
    #naj vrne graf ki ti pove kaj naj naredi poteza
        
    def izvedenapoteza(self,graf,poteza):
        
        a={}
        for i in graf:
            a[i]=graf[i]
            
        if poteza==[]:
            return a
        else:
            
            
            for i in poteza:
                a[i[1]]=(i[2],i[2])
            a[poteza[0][0]]=(1,0)
            return a
    #to je izvedena poteza z dodatno moznostjo da dodas vojake


    def makspoteza(self,graf,drzava):
        a=self.moznepoteze(drzava,graf)
        if len(a)==1:
            return a[0]
        elif len(a)==0:
            return []
        else:
            maks=a[0]
            b=1
            while b<len(a)-1:
                if self.tocke(self.izvedenapoteza(graf,a[b]))>self.tocke(self.izvedenapoteza(graf,maks)):
                    maks=a[b]
                b+=1
            return maks
    
    #dejansko izvede potezo
    def izvedipotezo(self,poteza):
        if poteza!=[]:
            for i in poteza:
                self.izbrana_drzava=i[0]
                self.stevec=i[2]
                self.napad(i[1])
            
        

    def sosede(self,graf,drzava):
        z=[]
        for i in self.mappa.Graf[drzava]:
            if i not in graf:
                z+=[i]
        return z
        


                        
        


            
            

    
    
        
        

    
    
                    
        
                    
                
                
                
                
        
        
        
        
            
            
            
            
                
                
                
                
                                     
                                     


        
    
        
        
        




    
                                
                        
                                    
                                
                                   
                                   
                                   
                                   
                                   
                                
                        
                            
                    
              
              
                  

            
            

       
            
    
            

root.title("risk")
aplikacija=Risk(root)
root.mainloop()
            
        

